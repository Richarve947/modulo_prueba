{
    'name': 'Módulo de prueba',
    'version': '14.0.1.0.0',
    'category': 'sale',
    'summary': """modulo de prueba""",
    'description': """
        
    """,
    'author': 'curso',
    'depends': ['sale_management'],
    'data': [
        'views/sale_order_views.xml',
    ],
    'license': "LGPL-3",
    'installable': True,
    'application': False,
}
