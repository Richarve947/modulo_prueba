from odoo import fields, models


class SaleOrder(models.Model):
    _inherit = 'sale.order'

    num_serie = fields.Char(string="Número de serie", )
    posible_cliente_id = fields.Many2one(comodel_name="res.partner", string="Posible cliente" )
    fecha_de_inicio = fields.Datetime(string="Fecha", default=fields.datetime.now(), )
